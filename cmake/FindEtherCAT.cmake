# Locate EtherCAT
#
# This module defines
#  EtherCAT_FOUND, if false, do not try to link to EtherCAT
#  EtherCAT_LIBRARY, where to find EtherCAT
#  EtherCAT_INCLUDE_DIR, where to find ecrt.h
#
# By default, the dynamic libraries of EtherCAT will be found. To find the static ones instead,
# you must set the EtherCAT_STATIC_LIBRARY variable to TRUE before calling find_package(EtherCAT ...).
#
# If EtherCAT is not installed in a standard path, you can use the EtherCAT_DIR CMake variable
# to tell CMake where EtherCAT is.

# attempt to find static library first if this is set
if(EtherCAT_STATIC_LIBRARY)
    set(EtherCAT_STATIC libethercat.a)
endif()

# find the EtherCAT include directory
find_path(EtherCAT_INCLUDE_DIR ecrt.h
          PATH_SUFFIXES include
          PATHS
          /opt/etherlab/
          ${EtherCAT_DIR}/include/)

# find the EtherCAT library
find_library(EtherCAT_LIBRARY
             NAMES ${EtherCAT_STATIC} libethercat.so
             PATH_SUFFIXES lib64 lib
             PATHS  /opt/etherlab
                    ${EtherCAT_DIR}/lib)

# handle the QUIETLY and REQUIRED arguments and set EtherCAT_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(EtherCAT DEFAULT_MSG EtherCAT_INCLUDE_DIR EtherCAT_LIBRARY)
mark_as_advanced(EtherCAT_INCLUDE_DIR EtherCAT_LIBRARY)
