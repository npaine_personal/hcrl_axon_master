# Locate parapin
#
# This module defines
#  parapin_FOUND, if false, do not try to link to parapin
#  parapin_LIBRARY, where to find libparapin.a
#  parapin_INCLUDE_DIR, where to find parapin.h
#
# If parapin is not installed in a standard path, you can use the parapin_DIR CMake variable
# to tell CMake where parapin is.

# find the parapin include directory
find_path(parapin_INCLUDE_DIR parapin.h
          PATH_SUFFIXES include
          PATHS
          /usr/local/
          ${parapin_DIR}/include/)

# find the parapin library
find_library(parapin_LIBRARY
             NAMES libparapin.a
             PATH_SUFFIXES lib64 lib
             PATHS  /usr/local/
                    ${parapin_DIR}/lib)

# handle the QUIETLY and REQUIRED arguments and set EtherCAT_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(parapin DEFAULT_MSG parapin_INCLUDE_DIR parapin_LIBRARY)
mark_as_advanced(parapin_INCLUDE_DIR parapin_LIBRARY)
