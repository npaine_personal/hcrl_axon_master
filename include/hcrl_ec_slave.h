/*
 * Unit_conversions.h
 *
 *  Created on: Mar 25, 2015
 *      Author: npaine
 */

#ifndef HCRL_EC_SLAVE_H_
#define HCRL_EC_SLAVE_H_

#include "hcrl_axon_common/ACT.h"

#define HCRLSlavePos 0, 0 // alias and position

#define HCRL_Slave 0x000005ff, 0x26483052 // vendor ID, product ID

void check_domain1_state(void);

/*****************************************************************************/

void check_master_state(void);

/*****************************************************************************/

void check_slave_config_states(void);

void updateLocalState(hcrl_axon_common::ACT *actMsg);


#endif /* HCRL_EC_SLAVE_H_ */