#ifndef ACTUATOR_HPP
#define ACTUATOR_HPP

typedef struct{
  //timing info
  float SampleTime_s;
  float SamplePeriod_s;

  //actuator state
    //kinematics
  float Motor_angle_rad;
  float Motor_vel_radps;
  //float Actuator_pos_m;
  //float Actuator_vel_mps;
  //float Arm_ang_rad;
  //float Arm_vel_radps;

    //dynamics
  //float Actuator_force_n;
  //float LoadCell_force_n;
  //float Arm_torque_nm;
  float Motor_torque_nm;

    //energetics
  float Motor_elec_power_w;
  float Motor_mech_power_w;
  //float Arm_power_w;
  //float Mech_efficiency;
  float Motor_efficiency;
  //float Total_efficiency;
  float Bus_voltage_V;
  float Bus_current_A;
  float Resevoir_temp_F;
  float MotorCase_temp_F;
  float Servo_temp_F;

  //desired control values
  float Motor_current_des_amps;
  //float Actuator_force_des_n;
  //float Actuator_pos_des_m;
  //float Actuator_vel_des_mps;
  //float Arm_ang_des_rad;
  //float Arm_vel_des_radps;

  //processing flag
  unsigned char newData;
} Actuator_state_str;

#endif //_ACTUATOR_HPP