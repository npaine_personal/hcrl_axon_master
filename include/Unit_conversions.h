/*
 * Unit_conversions.h
 *
 *  Created on: Mar 25, 2015
 *      Author: npaine
 */

#ifndef UNIT_CONVERSIONS_H_
#define UNIT_CONVERSIONS_H_

#include "autoCalTorque.h"
#include "autoCalCurrent.h"
#include "Parameters.h"

//math
#ifndef M_PI
#define M_PI 3.14159265359
#endif

//ADC
#define rawADC_2_volts(raw)  ( (float)raw*(ADC_REF_VOLTS/ADC_PRECISION_COUNTS) )

//DAC & current command
#define rawDAC_2_VDAC(raw) ( ( (float)(raw)-CURRENT_REF_ZERO_OFFSET_RAW)*DAC_VREF_V/DAC_PRECISION_COUNTS)
#define VDAC_2_Vref(vdac) (((float)(vdac)+CURRENT_REF_CIRCUIT_OFFSET)*CURRENT_REF_CIRCUIT_GAIN) //desired motor current circuitry
#define Vref_2_amps(vref) ((float)(vref)/(CURRENT_REF_VPOS-CURRENT_REF_VNEG)*MOTOR_AMP_MAX_CURRENT*2.0)
#define rawDAC_2_amps(raw) ( Vref_2_amps( VDAC_2_Vref( rawDAC_2_VDAC( raw ) ) ) )

#define amps_2_Vref(amps) ( (float)(amps)*(CURRENT_REF_VPOS-CURRENT_REF_VNEG)/MOTOR_AMP_MAX_CURRENT/2.0)
#define Vref_2_VDAC(vref) ((float)(vref)/CURRENT_REF_CIRCUIT_GAIN-CURRENT_REF_CIRCUIT_OFFSET)
#define VDAC_2_rawDAC(vdac) ((float)(vdac)*DAC_PRECISION_COUNTS/DAC_VREF_V+CURRENT_REF_ZERO_OFFSET_RAW)
#define amps_2_rawDAC(amps) (VDAC_2_rawDAC(Vref_2_VDAC(amps_2_Vref(amps))))
//#define amps_2_rawDAC(amps) ( ((float)(amps)*DAC_PRECISION_COUNTS/2/MOTOR_AMP_MAX_CURRENT)+DAC_PRECISION_COUNTS/2+CURRENT_REF_ZERO_OFFSET_RAW )


//Thermistor
#define volts_2_res(v) ( (float)(v)*THERMISTOR_CIRCUIT_RESISTOR/(THERMISTOR_CIRCUIT_SUPPLY*THERMISTOR_CIRCUIT_GAIN - (float)(v)) )
#define rawADC_2_F(raw)  (C_2_F(K_2_C(res_2_K(volts_2_res(rawADC_2_volts((raw))))) ) )
#define rawADC_2_C(raw)  (K_2_C(res_2_K(volts_2_res(rawADC_2_volts((raw))))) )
#define volts_2_C(v) (K_2_C(res_2_K(volts_2_res( v ) ) ) )

#define res_2_K(res) (1/(THERMISTOR_CONST_A + THERMISTOR_CONST_B * log((float)(res)) + THERMISTOR_CONST_C*pow( log( (float)(res) ),3 ) ) )

//Temperature
#define K_2_C(K) ((float)(K)-274.15)
#define C_2_F(C) ((float)(C)*1.8+32)

//time
#define usec_2_sec(us) ((float)(us)*(1/1000000.0))
#define msec_2_usec(ms) ((ms)*1000L)
#define msec_2_sec(ms) ((float)(ms)*(1/1000.0))
#define sec_2_msec(s) ((s)*1000L)
#define mhz_2_hz(mhz) ((mhz)*1000000L)
#define mhz_2_khz(mhz) ((mhz)*1000L)
#define sysClk_2_sec(ticks) ((float)(ticks)*SYS_CLK_PERIOD_SEC)

//bus voltage
#define volts_2_Vbus(v) ((v)*(1/VBUS_GAIN))
#define rawADC_2_Vbus(raw) (volts_2_Vbus(rawADC_2_volts(raw) ) )

//bus current
#define rawADC_2_Ibus(raw) ( ( (rawADC_2_volts(raw)-IBUS_CIRCUIT_OFFSET)/IBUS_CIRCUIT_GAIN - IBUS_SENSOR_OFFSET)/IBUS_SENSOR_GAIN)
#define Ibus_2_volts(ibus) (((ibus)*IBUS_SENSOR_GAIN + IBUS_SENSOR_OFFSET) * IBUS_CIRCUIT_GAIN + IBUS_CIRCUIT_OFFSET)

//angles
#define rad_2_deg(rad) ((float)(rad)*(180.0/M_PI))
#define rps_2_rpm(rps) ((float)(rps)*(30.0/M_PI))

//motor position
#define rawHall_2_rad(raw) ((float)(raw)*((M_PI/3.0)*2/MOTOR_NUM_POLES))
#define rawHall_2_deg(raw) (rad_2_deg(rawHall_2_rad(raw)))

//motor velocity
#define deltaT_2_radps(dt) (rawHall_2_rad(3.0)/sysClk_2_sec(dt)) //we're looking at one channel, the other two channels have both transitioned to get an hall state change on this one channel
#define deltaT_2_degps(dt) (rad_2_deg(deltaT_2_radps(dt) ) )
#define deltaT_2_rpm(dt) (rps_2_rpm(deltaT_2_radps(dt) ) )

//motor current
#define volts_2_Imotor(v) ( ( (v)-IMOTOR_CIRCUIT_OFFSET)*IMOTOR_SCALE_FACTOR*IMOTOR_DIRECTION/IMOTOR_CIRCUIT_GAIN )
#define rawADC_2_Imotor(raw) (volts_2_Imotor(rawADC_2_volts(raw) ) )
#define Imotor_2_volts(imotor) ((imotor)*IMOTOR_CIRCUIT_GAIN/IMOTOR_DIRECTION/IMOTOR_SCALE_FACTOR + IMOTOR_CIRCUIT_OFFSET)

//motor torque
#define rawADC_2_Tmotor(raw) ( ( ( ( ( (rawADC_2_volts(raw)-LC_CIRCUIT_STAGE2_OFFSET)/LC_CIRCUIT_STAGE2_GAIN ) - LC_CIRCUIT_STAGE1_OFFSET )/LC_CIRCUIT_STAGE1_GAIN ) * LC_RATED_TORQUE_NM * LC_DIRECTION / (LC_SENSITIVITY_VpV*LC_EXCITATION_V) ) + LC_TORQUE_OFFSET_NM)
#define Tmotor_2_volts(tmotor) ((((((tmotor) - LC_TORQUE_OFFSET_NM)*LC_SENSITIVITY_VpV*LC_EXCITATION_V/LC_DIRECTION/LC_RATED_TORQUE_NM*LC_CIRCUIT_STAGE1_GAIN) + LC_CIRCUIT_STAGE1_OFFSET)*LC_CIRCUIT_STAGE2_GAIN)+LC_CIRCUIT_STAGE2_OFFSET)


#endif /* UNIT_CONVERSIONS_H_ */
