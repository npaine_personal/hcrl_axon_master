/*
* autoCalTorque.h
*
*
*      Author: ec_autoCal_torque.cpp
*/

#ifndef AUTO_CAL_TORQUE_H_
#define AUTO_CAL_TORQUE_H_

#define CURRENT_OFFSET_AUTO_CAL -33
#define LC_AUTO_CAL 0.192197

#define VREF_2p5 2.556507

#endif /* AUTO_CAL_TORQUE_H_ */