# README #

# Install ROS #

The official instructions are here: http://wiki.ros.org/indigo/Installation/Ubuntu

    $ sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
    $ wget https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -O - | sudo apt-key add -
    $ sudo apt-get update
    $ sudo apt-get install ros-indigo-ros-base
    $ sudo rosdep init
    $ rosdep update
    $ sudo apt-get install python-rosinstall

# Create a ROS workspace #

A ROS workspace is required to hold ROS packages.

    $ source /opt/ros/indigo/setup.bash
    $ mkdir -p ~/act_ros_workspace/src
    $ cd ~/act_ros_workspace/src
    $ catkin_init_workspace
    $ cd ~/act_ros_workspace/
    $ catkin_make
    $ source devel/setup.bash

To automatically setup the workspace at login, edit ~/.bashrc and add the following:

    # Setup Actuator Component Testbed ROS workspace
    export ROS_MASTER_URI=http://localhost:11311
    export ROS_IP=192.168.1.110                         # Ensure this matches the ACT PC's IP address
    source $HOME/act_ros_workspace/devel/setup.bash

# Add hcrl_axon_master package to ROS workspace

To add the hcrl_axon_master package to your ROS workspace:

    $ cd ~/act_ros_workspace/src
    $ git clone git@bitbucket.org:npaine/hcrl_axon_master.git
    $ cd hcrl_axon_master/
    $ git checkout feature/rosified

To build the package:
    
    $ cd ~/act_ros_workspace
    $ catkin_make

To clean-build the package and enable verbose mode (useful for debugging purposes):

    $ cd ~/act_ros_workspace
    $ rm -rf build devel
    $ reset
    $ VERBOSE=true catkin_make

# Execution Instructions

Open two terminals.  In the first terminal, start roscore:

    $ roscore

In the second terminal, start the program:

    $ roscd     // This should change the working directory to be /act_ros_workspace/devel
    $ sudo -E ./lib/hcrl_axon_master/ec_printState

Note that the above command containing "sudo" is just temporary until we figure out how to allow non-root users to toggle the parallel port pins.

# Modify EtherCAT driver to allow non-root access

See section 9.5 of the user's manual.

# Previous Instructions #
First install the ethercat master by following these instructions: http://conf.pienet.pie.com.hk:8090/display/erockhead/How+to+install+Etherlab+Master

To run the test:

    $ cd [path to this repo]
    $ make clean
    $ make
    $ sudo ./ec_printState

To view the ROS topic data:

    $ rostopic echo /actData
    $ rqt_plot /actData/MotorCase_temp_F  # plots the motor case temperature
