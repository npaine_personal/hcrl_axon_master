#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h> 

/****************************************************************************/

#include "ros/ros.h"
#include "hcrl_axon_common/ACT.h"

/****************************************************************************/

#include "parapin.h"

/****************************************************************************/

#include "ecrt.h"

/****************************************************************************/

#include <sys/time.h>  // for gettimeofday(...)
#include "Unit_conversions.h"
#include <math.h>

#include "actuator.hpp"
#include "hcrl_ec_slave.h"

/****************************************************************************/

// Application parameters
#define DEFAULT_SAMPLE_FREQUENCY 100
#define PRIORITY 1
#define ENABLE_PRINTF 0
#define DEFAULT_CURRENT 2.0
#define DEFAULT_SIGNAL_FREQUENCY 0.1//0.5 //sine wave frequency
#define WAIT_TIME_S 5.0
#define BLEND_TIME_S 2.0

/****************************************************************************/

// Timer
static unsigned int sig_alarms = 0;
static unsigned int user_alarms = 0;

/****************************************************************************/

float signal_frequency = DEFAULT_SIGNAL_FREQUENCY;

enum {CONST_WAVE, SINE_WAVE};

//unsigned int current_offset_raw = 55;
static int SampleFreq_Hz = DEFAULT_SAMPLE_FREQUENCY;
static int DataLogging = 0;
static int ROSTopicPublishing = 1;
static int Waveform = CONST_WAVE;
static float CurrentAmplitude_A = DEFAULT_CURRENT;

// for call to ecrt_slave_config_reg_pdo_entry(...)


static unsigned char End=0;
static void endme(int dummy) { End=1; }

//file stuff
#define LogFile_DEFAULT "act_data.csv" //tab-separated values
#define FILE_DELIMITER ","
static char *LogFile;
static char File_default[] = LogFile_DEFAULT;

static unsigned int seqno = 0;
static unsigned int lastSeqno = 1;

static struct timeval sendTime, rcvTime, startTime;

static struct timeval callRcvTime, callProcessTime, callReadWriteTime, callQueueTime, callSendTime, doneTime;

static float max_latency_ms=0.0, min_latency_ms=1000.0, time_lastData_s = 0.0;

// ********************extern ethercat stuff********************
// process data
extern uint8_t *domain1_pd;

extern ec_master_t *master;
extern ec_master_state_t master_state;

extern ec_domain_t *domain1;
extern ec_domain_state_t domain1_state;

extern ec_slave_config_t *slave_config;
extern ec_slave_config_state_t slave_config_state;

extern unsigned int bit_position;

extern unsigned int off_loopPlusOne;
extern unsigned int off_v1_raw;
extern unsigned int off_v2_raw;
extern unsigned int off_v3_raw;
extern unsigned int off_v4_raw;
extern unsigned int off_v5_raw;
extern unsigned int off_v6_raw;
extern unsigned int off_v7_raw;
extern unsigned int off_v8_raw;
extern unsigned int off_hall_dt_low;
extern unsigned int off_hall_dt_high;
extern unsigned int off_hall_position_raw;
extern unsigned int off_motorCoreTempEst_C_low;
extern unsigned int off_motorCoreTempEst_C_high;
extern unsigned int off_faults;

extern unsigned int off_current_des_raw;
extern unsigned int off_loop;

extern ec_pdo_entry_reg_t domain1_regs[];
extern ec_pdo_entry_info_t slave_0_pdo_entries[];
extern ec_pdo_info_t slave_0_pdos[];
extern ec_sync_info_t slave_0_syncs[];
/*****************************************************************************/

//#if ENABLE_PRINTF
//static struct timeval lastCycleTime;
//#endif


/*!
 * Computes the time between two timestamps in milliseconds.
 */
double computeDeltaTime(struct timeval * time1, struct timeval * time2)
{
    return 1000 * (time2->tv_sec - time1->tv_sec) + (time2->tv_usec - time1->tv_usec) / 1000.0;
}

/*****************************************************************************/

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -f <log frequency (hz)>    : sample freq (default :%d)\n", SampleFreq_Hz);
  printf ("  -l                         : enable data logging (default: %d)\n", DataLogging);
  printf ("  -s                         : use a sine waveform (default: %d)\n", Waveform);
  printf ("  -a <current amplitude (A)> : set current amplitude (default: %f)\n", CurrentAmplitude_A);
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "f:a:lsh?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'f':
        SampleFreq_Hz = atoi(optarg);
        break;
      case 'a':
        CurrentAmplitude_A = atof(optarg);
        break;
      case 'l':
        DataLogging = 1;
        break;
      case 's':
        Waveform = SINE_WAVE;
        break;
      case '?': // help
      case 'h': // help
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args

/****************************************************************************/
void cyclic_task(hcrl_axon_common::ACT *actMsg)
{
    //unsigned long sigTime_us;
    unsigned int v1_raw;
    unsigned int v2_raw;
    unsigned int v3_raw;
    unsigned int v4_raw;
    unsigned int v5_raw;
    unsigned int v6_raw;
    unsigned int v7_raw;
    unsigned int v8_raw;
    unsigned int hall_dt_low;
    unsigned int hall_dt_high;
    int hall_dt;
    int hall_position_raw;
    unsigned int motorCoreTempEst_C_low;
    unsigned int motorCoreTempEst_C_high;
    int motorCoreTempEst_C_tmp;
    unsigned int faults;
    
    unsigned int current_des_raw;

    // receive process data
    gettimeofday(&callRcvTime, NULL);
    ecrt_master_receive(master);

    actMsg->SampleTime_s = callRcvTime.tv_sec - startTime.tv_sec + usec_2_sec(callRcvTime.tv_usec - startTime.tv_usec);
    actMsg->SamplePeriod_s = callRcvTime.tv_sec - callProcessTime.tv_sec + usec_2_sec(callRcvTime.tv_usec - callProcessTime.tv_usec);

    gettimeofday(&callProcessTime, NULL);
    ecrt_domain_process(domain1);

    gettimeofday(&callReadWriteTime, NULL);

    //Part 1: Do timing/latency analysis
    if (lastSeqno == seqno + 1)
    {
        seqno++;
        gettimeofday(&sendTime, NULL);
        set_pin(LP_PIN[1]);
        // write process data
        //printf("------\nCYCLIC_TASK: Writing value %u\n", seqno);
        EC_WRITE_U16(domain1_pd + off_loop, seqno);
    }

    // read process data
    updateLocalState(actMsg);
    lastSeqno = EC_READ_U16(domain1_pd + off_loopPlusOne);

    if (lastSeqno == (seqno + 1))
    {
        clear_pin(LP_PIN[1]);
        gettimeofday(&rcvTime, NULL);

        double t = 1000 * (rcvTime.tv_sec - sendTime.tv_sec) + (rcvTime.tv_usec - sendTime.tv_usec) / 1000.0;
        printf("CYCLIC_TASK: round trip latency: %f ms\n", t);
        if(seqno > 5){ //skip the first few cycles
            if(t > max_latency_ms) max_latency_ms = t;
            if(t < min_latency_ms) min_latency_ms = t;
        }
        time_lastData_s = actMsg->SampleTime_s;
    }

    //Part 2: Send/receive application data
    if(actMsg->SampleTime_s < WAIT_TIME_S){ //wait for disabling e-stop
        actMsg->Motor_current_des_A = 0;
    } else{
        if(Waveform == SINE_WAVE) {
            actMsg->Motor_current_des_A = CurrentAmplitude_A*sin(actMsg->SampleTime_s*2*M_PI*signal_frequency);
    
        } else {
            actMsg->Motor_current_des_A = CurrentAmplitude_A;
        }

        if(actMsg->SampleTime_s < WAIT_TIME_S + BLEND_TIME_S) { //if just starting, blend output
            actMsg->Motor_current_des_A = (actMsg->SampleTime_s - WAIT_TIME_S)/BLEND_TIME_S * actMsg->Motor_current_des_A;
        }
    } 

    current_des_raw = amps_2_rawDAC(actMsg->Motor_current_des_A);
    EC_WRITE_U16(domain1_pd + off_current_des_raw, current_des_raw );

    

    printf("CYCLIC_TASK: Time : %f\n",actMsg->SampleTime_s);
    
    // send process data
    gettimeofday(&callQueueTime, NULL);
    ecrt_domain_queue(domain1);

    gettimeofday(&callSendTime, NULL);

    ecrt_master_send(master);         // sends all datagrams in the queue

    gettimeofday(&doneTime, NULL);

    //more timing/latency measurement
#if ENABLE_PRINTF
    if (lastCycleTime.tv_sec != 0 && lastCycleTime.tv_usec != 0)
    {

        printf("CYCLIC_TASK: cycle period: %f ms\n",
            computeDeltaTime(&lastCycleTime, &callRcvTime));
    }
    lastCycleTime = callRcvTime;

    printf("CYCLIC_TASK: internal latencies (us): %f\t%f\t%f\t%f\t%f\t%f\n",
        1000 * computeDeltaTime(&callRcvTime, &callProcessTime),
        1000 * computeDeltaTime(&callProcessTime, &callReadWriteTime),
        1000 * computeDeltaTime(&callReadWriteTime, &callQueueTime),
        1000 * computeDeltaTime(&callQueueTime, &callSendTime),
        1000 * computeDeltaTime(&callSendTime, &doneTime),
        1000 * computeDeltaTime(&callRcvTime, &doneTime));
#endif

}

/****************************************************************************/

void signal_handler(int signum) {
    switch (signum) {
        case SIGALRM:
            sig_alarms++;
            break;
    }
}

void logHeader(FILE* ifp){
  fprintf(ifp, "Time (sec)" FILE_DELIMITER 
    "Sample period (sec)" FILE_DELIMITER  
    "Faults" FILE_DELIMITER  
    "Desired motor current (A)" FILE_DELIMITER 
    "Measured motor current (A)" FILE_DELIMITER 
    "Motor torque (Nm)" FILE_DELIMITER
    "Motor angle (rad)" FILE_DELIMITER
    "Motor velocity (rpm)" FILE_DELIMITER
    "Motor mech power (W)" FILE_DELIMITER
    "Motor case temperature (F)" FILE_DELIMITER
    "Motor core temperature (F)" FILE_DELIMITER
    "Servo temperature (F)" FILE_DELIMITER
    "Resevoir temperature (F)" FILE_DELIMITER
    "Bus voltage (V)" FILE_DELIMITER
    "Bus current (A)" FILE_DELIMITER
    "Battery power (W)" FILE_DELIMITER
    "Voltage Reference (V)\n");
}

void logData(FILE* ifp, hcrl_axon_common::ACT *actMsg){
  fprintf(ifp, "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%u" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER 
    "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER 
    "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f" FILE_DELIMITER "%f\n", 
          actMsg->SampleTime_s, 
          actMsg->SamplePeriod_s, 
          actMsg->Faults, 
          actMsg->Motor_current_des_A, 
          actMsg->Motor_current_meas_A, 
          actMsg->Motor_torque_nm,
          actMsg->Motor_angle_rad, 
          actMsg->Motor_vel_rpm, 
          actMsg->Motor_mech_power_W,
          actMsg->MotorCase_temp_F,
          actMsg->MotorCore_temp_F,
          actMsg->Servo_temp_F,
          actMsg->Resevoir_temp_F,
          actMsg->Bus_voltage_V,
          actMsg->Bus_current_A,
          actMsg->Battery_power_W,
          actMsg->VoltRef2p5);
}

/****************************************************************************/

int main(int argc, char **argv)
{

    // ec_slave_config_t *sc;
    struct sigaction sa;
    struct itimerval tv;

    signal(SIGINT, endme); //ctrl-c out gracefully

    parse_args(argc,argv);

    ros::init(argc, argv, "listener");
    ros::NodeHandle nh;
    ros::Publisher actPublisher = nh.advertise<hcrl_axon_common::ACT>("actData", 1000);
    hcrl_axon_common::ACT actMsg;

    //file stuff
    LogFile = File_default;
    FILE* ifp = 0; //set to zero to remove compiler warning
    if(DataLogging){
        ifp = fopen(LogFile, "w");
        if (!ifp) {
          fprintf(stderr, "Cannot open file %s\n", LogFile);
          exit(1);
        }
    
        //print header
        printf("Data logging to "); 
        printf("%s ", LogFile); 
        logHeader(ifp);
        printf("at %d hz\n", DEFAULT_SAMPLE_FREQUENCY);
    }

    printf("MAIN: Desired cyclic_task execution frequency: %i\n", SampleFreq_Hz);
 
    // Initialize the parallel port as a GPIO interface
    if (pin_init_user(LPT1) < 0)
        exit(0);

    pin_output_mode(LP_DATA_PINS | LP_SWITCHABLE_PINS);

    // Reserve an EtherCAT master for exclusive use
    printf("MAIN: Reserving master...\n");
    master = ecrt_request_master(0); // 0 is the index of the master
    if (!master)
        return -1;

    // A domain is required for process data exchange
    printf("MAIN: Creating domain...\n");
    domain1 = ecrt_master_create_domain(master);
    if (!domain1)
        return -1;

    // Obtain a slave configuration
    printf("MAIN: Obtaining slave configuration...\n");
    if (!(slave_config = ecrt_master_slave_config(master,
         HCRLSlavePos, // alias, position
         HCRL_Slave))) // vendor ID, product ID
    {
        fprintf(stderr, "MAIN: Failed to get slave configuration\n");
        return -1;
    }
    
    printf("MAIN: Registering PDO entries...\n");
    if (ecrt_domain_reg_pdo_entry_list(domain1, domain1_regs)) 
    {
        fprintf(stderr, "MAIN: PDO entry registration failed!\n");
        return -1;
    }

    printf("MAIN: Configuring PDOs...\n");
    if (ecrt_slave_config_pdos(slave_config, EC_END, slave_0_syncs)) {
        fprintf(stderr, "MAIN: Failed to configure PDOs.\n");
        return -1;
    }

    printf("MAIN: Activating master...\n");
    if (ecrt_master_activate(master))             // see line 919 of ecrt.h
        return -1;

    printf("MAIN: Obtaining the domain's process data\n"); // see line 1687 of ecrt.h
    if (!(domain1_pd = ecrt_domain_data(domain1))) {  
        return -1;
    }

#if PRIORITY
    printf("MAIN: Setting priority to be -19...\n");
    pid_t pid = getpid();
    if (setpriority(PRIO_PROCESS, pid, -19))
        fprintf(stderr, "MAIN: Warning: Failed to set priority: %s\n",
                strerror(errno));
#endif

    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGALRM, &sa, 0)) {
        fprintf(stderr, "MAIN: Failed to install signal handler!\n");
        return -1;
    }

    printf("MAIN: Starting timer...\n");
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = 1000000 / SampleFreq_Hz;
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = 1000;
    if (setitimer(ITIMER_REAL, &tv, NULL)) {
        fprintf(stderr, "MAIN: Failed to start timer: %s\n", strerror(errno));
        return 1;
    }

    printf("MAIN: Started.\n");
    gettimeofday(&startTime, NULL);
    while (!End && ros::ok()) {
        pause();

#if 0
        struct timeval t;
        gettimeofday(&t, NULL);
        printf("%u.%06u\n", t.tv_sec, t.tv_usec);
#endif

        while (sig_alarms != user_alarms && !End) {
            cyclic_task(&actMsg);
            if(DataLogging) { logData(ifp, &actMsg); }
            if (ROSTopicPublishing)
            {
                actMsg.header.stamp = ros::Time::now();
                actPublisher.publish(actMsg);
            }

            ros::spinOnce();
            user_alarms++;
        }
    }

    printf("Max latency (ms) = %f\nMin latency (ms) = %f\nLast data written to slave at %f sec\n", max_latency_ms, min_latency_ms, time_lastData_s);

    if(DataLogging){
        printf("ending...\ndata written to: ");
        printf("%s\n", LogFile);
        fclose(ifp);
    }

    return 0;
}

/****************************************************************************/
