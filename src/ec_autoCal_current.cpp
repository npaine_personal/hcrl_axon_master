#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h> 

/****************************************************************************/

#include "ros/ros.h"
#include "hcrl_axon_common/ACT.h"

/****************************************************************************/

#include "parapin.h"

/****************************************************************************/

#include "ecrt.h"

/****************************************************************************/

#include <sys/time.h>  // for gettimeofday(...)
#include "Unit_conversions.h"
#include <math.h>
#include "Parameters.h"
#include "autoCalCurrent.h"

#include "actuator.hpp"
#include "hcrl_ec_slave.h"

/****************************************************************************/

// Application parameters
#define DEFAULT_SAMPLE_FREQUENCY 100
#define PRIORITY 1
#define ENABLE_PRINTF 0
#define INITIAL_PAUSE_S 5.0

/****************************************************************************/

// Timer
static unsigned int sig_alarms = 0;
static unsigned int user_alarms = 0;

/****************************************************************************/

//unsigned int current_offset_raw = 55;
static int SampleFreq_Hz = DEFAULT_SAMPLE_FREQUENCY;
static int ROSTopicPublishing = 1;
static float calibrateStartTime_s = 0;
static float calibrateAveragingTime_s = 2.0; //time to add up analog values
static unsigned int calibrateFirstRun = 1;
static float Bus_current_A_acc=0, Motor_current_meas_A_acc=0, Motor_torque_nm_acc=0;
static float Bus_current_A_avg=0, Motor_current_meas_A_avg=0, Motor_torque_nm_avg=0;
static int NumSamples=0;
static int SuccessfulCalibration=0;

// for call to ecrt_slave_config_reg_pdo_entry(...)


static unsigned char End=0;
static void endme(int dummy) { End=1; }

//file stuff
#define LogFile_DEFAULT "/home/npaine/act_ros_workspace/src/hcrl_axon_master/include/autoCalCurrent.h" //tab-separated values
#define FILE_DELIMITER ","
static char *LogFile;
static char File_default[] = LogFile_DEFAULT;

static unsigned int seqno = 0;
static unsigned int lastSeqno = 1;

static struct timeval sendTime, rcvTime, startTime;

static struct timeval callRcvTime, callProcessTime, callReadWriteTime, callQueueTime, callSendTime, doneTime;

static float max_latency_ms=0.0, min_latency_ms=1000.0;

// ********************extern ethercat stuff********************
// process data
extern uint8_t *domain1_pd;

extern ec_master_t *master;
extern ec_master_state_t master_state;

extern ec_domain_t *domain1;
extern ec_domain_state_t domain1_state;

extern ec_slave_config_t *slave_config;
extern ec_slave_config_state_t slave_config_state;

extern ec_pdo_entry_reg_t domain1_regs[];
extern ec_pdo_entry_info_t slave_0_pdo_entries[];
extern ec_pdo_info_t slave_0_pdos[];
extern ec_sync_info_t slave_0_syncs[];

extern unsigned int off_loopPlusOne;
extern unsigned int off_loop;
extern unsigned int off_current_des_raw;
/*****************************************************************************/

//#if ENABLE_PRINTF
//static struct timeval lastCycleTime;
//#endif


/*!
 * Computes the time between two timestamps in milliseconds.
 */
double computeDeltaTime(struct timeval * time1, struct timeval * time2)
{
    return 1000 * (time2->tv_sec - time1->tv_sec) + (time2->tv_usec - time1->tv_usec) / 1000.0;
}

/*****************************************************************************/

static void print_usage(int argc, char** argv)
{
  printf ("USAGE: %s [options] \n\n", *argv);
  printf ("Where [options] can be: \n");
  printf ("  -f <log frequency (hz)>    : sample freq (default :%d)\n", SampleFreq_Hz);
} // end print_usage

static int parse_args(int argc, char** argv)
{
  // set the flags
  const char* optflags = "f:h?";
  int ch;

  // use getopt to parse the flags
  while(-1 != (ch = getopt(argc, argv, optflags))){
    switch(ch){
      // case values must match long_options
      case 'f':
        SampleFreq_Hz = atoi(optarg);
        break;
      case '?': // help
      case 'h': // help
      default:  // unknown
        print_usage(argc, argv);
        exit (-1);
    }
  }
  return (0);
} // end parse_args


//1 if outside of range, 0 otherwise
int outsideOfRange(float value, float max, float min){
    if(value > max || value < min){
        return 1;
    } else{
        return 0;
    }
}

//returns 1 if everything looks ok, 0 if something can't be right
int checkForSaneValues(hcrl_axon_common::ACT *actMsg){
    if( outsideOfRange(actMsg->Bus_current_A, SANE_CURRENT_MAX_A, SANE_CURRENT_MIN_A) ){
        return 0;
    }

    if( outsideOfRange(actMsg->Motor_current_meas_A, SANE_CURRENT_MAX_A, SANE_CURRENT_MIN_A) ){
        return 0;
    }

    if( outsideOfRange(actMsg->Motor_vel_rpm, SANE_MOTORVEL_MAX_RPM, SANE_MOTORVEL_MIN_RPM) ){
        return 0;
    }

    if( outsideOfRange(actMsg->Resevoir_temp_F, C_2_F(SANE_TEMP_MAX_C), C_2_F(SANE_TEMP_MIN_C) ) ){
        return 0;
    }

    if( outsideOfRange(actMsg->Servo_temp_F, C_2_F(SANE_TEMP_MAX_C), C_2_F(SANE_TEMP_MIN_C) ) ){
        return 0;
    }

    if( outsideOfRange(actMsg->MotorCase_temp_F, C_2_F(SANE_TEMP_MAX_C), C_2_F(SANE_TEMP_MIN_C) ) ){
        return 0;
    }

    return 1;
}

void writeToFile(float ibus_auto_cal, float imotor_auto_cal){
    //file stuff
    LogFile = File_default;
    FILE* ifp = 0; //set to zero to remove compiler warning

    ifp = fopen(LogFile, "w");
    if (!ifp) {
      fprintf(stderr, "Cannot open file %s\n", LogFile);
      exit(1);
    }

    fprintf(ifp, "/*\n* autoCalCurrent.h\n*\n*\n*      Author: ec_autoCal_current.cpp\n*/\n\n");
    fprintf(ifp, "#ifndef AUTO_CAL_CURRENT_H_\n#define AUTO_CAL_CURRENT_H_\n\n");
    fprintf(ifp, "#define IBUS_AUTO_CAL %f\n", ibus_auto_cal);
    fprintf(ifp, "#define IMOTOR_AUTO_CAL %f\n", imotor_auto_cal);
    //fprintf(ifp, "#define LC_AUTO_CAL %f\n\n", lc_auto_cal);
    fprintf(ifp, "#endif /* AUTO_CAL_CURRENT_H_ */");

    printf("ending...\ndata written to: ");
    printf("%s\n", LogFile);
    fclose(ifp);
    SuccessfulCalibration = 1;
}

int calibrateAnalogSensors(hcrl_axon_common::ACT *actMsg){

    float ibus_offset, imotor_offset, tmotor_offset; 

    if(!checkForSaneValues(actMsg)){
        calibrateFirstRun = 1;
        printf("Invalid Data!!\n");
        return 0; //wait for good data before starting
    }

    if(calibrateFirstRun){
        calibrateStartTime_s = actMsg->SampleTime_s;
        calibrateFirstRun = 0;
        NumSamples = 0;
        Bus_current_A_acc = 0;
        Motor_current_meas_A_acc = 0;
        //Motor_torque_nm_acc = 0;
    }


    if(actMsg->SampleTime_s < calibrateAveragingTime_s + calibrateStartTime_s){

        //actMsg->Bus_voltage_V = 0;
        Bus_current_A_acc += actMsg->Bus_current_A;
        Motor_current_meas_A_acc += actMsg->Motor_current_meas_A;
        //Motor_torque_nm_acc += actMsg->Motor_torque_nm;
        NumSamples++;

        return 0;
    } else {
        Bus_current_A_avg = Bus_current_A_acc/NumSamples;
        Motor_current_meas_A_avg = Motor_current_meas_A_acc/NumSamples;
        //Motor_torque_nm_avg = Motor_torque_nm_acc/NumSamples;



        printf("Average values:\n");
        printf(" Bus Current (A): %f\n", Bus_current_A_avg);
        printf(" Motor Current (A): %f\n", Motor_current_meas_A_avg);
        //printf(" Motor Torque (Nm): %f\n\n", Motor_torque_nm_avg);

        ibus_offset = Ibus_2_volts(Bus_current_A_avg) - Ibus_2_volts(0.0);
        imotor_offset = Imotor_2_volts(Motor_current_meas_A_avg) - Imotor_2_volts(0.0);
        tmotor_offset = Tmotor_2_volts(Motor_torque_nm_avg) - Tmotor_2_volts(0.0);

        printf("Adjustments (these offsets will be added to the following):\n");
        printf(" Bus Current (volts, IBUS_CIRCUIT_OFFSET): %f (was %f)\n", IBUS_AUTO_CAL+ibus_offset, IBUS_AUTO_CAL );
        printf(" Motor Current (volts, IMOTOR_CIRCUIT_OFFSET): %f (was %f)\n", IMOTOR_AUTO_CAL+imotor_offset, IMOTOR_AUTO_CAL );
        //printf(" Motor Torque (volts, LC_CIRCUIT_STAGE2_OFFSET): %f\n", LC_AUTO_CAL+tmotor_offset );

        writeToFile(IBUS_AUTO_CAL+ibus_offset, IMOTOR_AUTO_CAL+imotor_offset);

        return 1;
    }

}

/****************************************************************************/
void cyclic_task(hcrl_axon_common::ACT *actMsg)
{
    unsigned int current_des_raw;

    // receive process data
    gettimeofday(&callRcvTime, NULL);
    ecrt_master_receive(master);

    actMsg->SampleTime_s = callRcvTime.tv_sec - startTime.tv_sec + usec_2_sec(callRcvTime.tv_usec - startTime.tv_usec);
    actMsg->SamplePeriod_s = callRcvTime.tv_sec - callProcessTime.tv_sec + usec_2_sec(callRcvTime.tv_usec - callProcessTime.tv_usec);

    gettimeofday(&callProcessTime, NULL);
    ecrt_domain_process(domain1);

    gettimeofday(&callReadWriteTime, NULL);

    //Part 1: Do timing/latency analysis
    if (lastSeqno == seqno + 1)
    {
        seqno++;
        gettimeofday(&sendTime, NULL);
        set_pin(LP_PIN[1]);
        // write process data
        //printf("------\nCYCLIC_TASK: Writing value %u\n", seqno);
        EC_WRITE_U16(domain1_pd + off_loop, seqno);
    }

    // read process data
    updateLocalState(actMsg);
    lastSeqno = EC_READ_U16(domain1_pd + off_loopPlusOne);

    if (lastSeqno == (seqno + 1))
    {
        clear_pin(LP_PIN[1]);
        gettimeofday(&rcvTime, NULL);

        double t = 1000 * (rcvTime.tv_sec - sendTime.tv_sec) + (rcvTime.tv_usec - sendTime.tv_usec) / 1000.0;
        printf("CYCLIC_TASK: round trip latency: %f ms\n", t);
        if(seqno > 5){ //skip the first few cycles
            if(t > max_latency_ms) max_latency_ms = t;
            if(t < min_latency_ms) min_latency_ms = t;
        }

        if(actMsg->SampleTime_s > INITIAL_PAUSE_S){
            if( calibrateAnalogSensors(actMsg) ){
                End = 1;
            }
        }
        else{
            printf("Waiting...release e-stop\n");
        }
    }

    //Part 2: Send/receive application data
    current_des_raw = amps_2_rawDAC(0.0);
    EC_WRITE_U16(domain1_pd + off_current_des_raw, current_des_raw );

    printf("CYCLIC_TASK: Time : %f\n",actMsg->SampleTime_s);
    
    // send process data
    ecrt_domain_queue(domain1);
    ecrt_master_send(master);         // sends all datagrams in the queue

    

}

/****************************************************************************/

void signal_handler(int signum) {
    switch (signum) {
        case SIGALRM:
            sig_alarms++;
            break;
    }
}

/****************************************************************************/

int main(int argc, char **argv)
{

    // ec_slave_config_t *sc;
    struct sigaction sa;
    struct itimerval tv;

    signal(SIGINT, endme); //ctrl-c out gracefully

    parse_args(argc,argv);

    ros::init(argc, argv, "listener");
    ros::NodeHandle nh;
    ros::Publisher actPublisher = nh.advertise<hcrl_axon_common::ACT>("actData", 1000);
    hcrl_axon_common::ACT actMsg;    

    printf("MAIN: Desired cyclic_task execution frequency: %i\n", SampleFreq_Hz);
 
    // Initialize the parallel port as a GPIO interface
    if (pin_init_user(LPT1) < 0)
        exit(0);

    pin_output_mode(LP_DATA_PINS | LP_SWITCHABLE_PINS);

    // Reserve an EtherCAT master for exclusive use
    printf("MAIN: Reserving master...\n");
    master = ecrt_request_master(0); // 0 is the index of the master
    if (!master)
        return -1;

    // A domain is required for process data exchange
    printf("MAIN: Creating domain...\n");
    domain1 = ecrt_master_create_domain(master);
    if (!domain1)
        return -1;

    // Obtain a slave configuration
    printf("MAIN: Obtaining slave configuration...\n");
    if (!(slave_config = ecrt_master_slave_config(master,
         HCRLSlavePos, // alias, position
         HCRL_Slave))) // vendor ID, product ID
    {
        fprintf(stderr, "MAIN: Failed to get slave configuration\n");
        return -1;
    }
    
    printf("MAIN: Registering PDO entries...\n");
    if (ecrt_domain_reg_pdo_entry_list(domain1, domain1_regs)) 
    {
        fprintf(stderr, "MAIN: PDO entry registration failed!\n");
        return -1;
    }

    printf("MAIN: Configuring PDOs...\n");
    if (ecrt_slave_config_pdos(slave_config, EC_END, slave_0_syncs)) {
        fprintf(stderr, "MAIN: Failed to configure PDOs.\n");
        return -1;
    }

    printf("MAIN: Activating master...\n");
    if (ecrt_master_activate(master))             // see line 919 of ecrt.h
        return -1;

    printf("MAIN: Obtaining the domain's process data\n"); // see line 1687 of ecrt.h
    if (!(domain1_pd = ecrt_domain_data(domain1))) {  
        return -1;
    }

#if PRIORITY
    printf("MAIN: Setting priority to be -19...\n");
    pid_t pid = getpid();
    if (setpriority(PRIO_PROCESS, pid, -19))
        fprintf(stderr, "MAIN: Warning: Failed to set priority: %s\n",
                strerror(errno));
#endif

    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if (sigaction(SIGALRM, &sa, 0)) {
        fprintf(stderr, "MAIN: Failed to install signal handler!\n");
        return -1;
    }

    printf("MAIN: Starting timer...\n");
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = 1000000 / SampleFreq_Hz;
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = 1000;
    if (setitimer(ITIMER_REAL, &tv, NULL)) {
        fprintf(stderr, "MAIN: Failed to start timer: %s\n", strerror(errno));
        return 1;
    }

    printf("MAIN: Started.\n");
    gettimeofday(&startTime, NULL);
    while (!End && ros::ok()) {
        pause();

#if 0
        struct timeval t;
        gettimeofday(&t, NULL);
        printf("%u.%06u\n", t.tv_sec, t.tv_usec);
#endif

        while (sig_alarms != user_alarms && !End) {
            cyclic_task(&actMsg);
            //if(DataLogging) { logData(ifp, &actMsg); }
            if (ROSTopicPublishing)
            {
                actMsg.header.stamp = ros::Time::now();
                actPublisher.publish(actMsg);
            }

            ros::spinOnce();
            user_alarms++;
        }
    }

    //printf("Max latency (ms) = %f\nMin latency (ms) = %f\n", max_latency_ms, min_latency_ms);


    
    

    return 0;
}

/****************************************************************************/
