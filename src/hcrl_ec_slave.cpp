
#include "hcrl_ec_slave.h"
#include "ecrt.h"
#include <stdio.h>
#include "hcrl_axon_common/ACT.h"
#include "Unit_conversions.h"

// process data
uint8_t *domain1_pd = NULL;

// EtherCAT
ec_master_t *master = NULL;
ec_master_state_t master_state = {};

ec_domain_t *domain1 = NULL;
ec_domain_state_t domain1_state = {};

ec_slave_config_t *slave_config = NULL;
ec_slave_config_state_t slave_config_state = {};

// offsets for PDO entries
//static unsigned int off_counter;
//static unsigned int off_counter_out;

unsigned int off_loopPlusOne;
unsigned int off_v1_raw;
unsigned int off_v2_raw;
unsigned int off_v3_raw;
unsigned int off_v4_raw;
unsigned int off_v5_raw;
unsigned int off_v6_raw;
unsigned int off_v7_raw;
unsigned int off_v8_raw;
unsigned int off_hall_dt_low;
unsigned int off_hall_dt_high;
unsigned int off_hall_position_raw;
unsigned int off_motorCoreTempEst_C_low;
unsigned int off_motorCoreTempEst_C_high;
unsigned int off_faults;

unsigned int off_current_des_raw;
unsigned int off_loop;

unsigned int bit_position = 0;

//const   
ec_pdo_entry_reg_t domain1_regs[] = {  // liang made this not a const since it was not showing up in the generated ~/act_ros_workspace/build/hcrl_axon_master/CMakeFiles/ec_printState.dir/src/hcrl_ec_slave.cpp.o file.
    //MISO
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x01,          // PDO entry subindex
        &off_loopPlusOne,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x02,          // PDO entry subindex
        &off_v1_raw,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x03,          // PDO entry subindex
        &off_v2_raw,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x04,          // PDO entry subindex
        &off_v3_raw,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x05,          // PDO entry subindex
        &off_v4_raw,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x06,          // PDO entry subindex
        &off_v5_raw,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x07,          // PDO entry subindex
        &off_v6_raw,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x08,          // PDO entry subindex
        &off_v7_raw,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x09,          // PDO entry subindex
        &off_v8_raw,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x0A,          // PDO entry subindex
        &off_hall_dt_low,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x0B,          // PDO entry subindex
        &off_hall_dt_high,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x0C,          // PDO entry subindex
        &off_hall_position_raw,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x0D,          // PDO entry subindex
        &off_motorCoreTempEst_C_low,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x0E,          // PDO entry subindex
        &off_motorCoreTempEst_C_high,
        &bit_position},          // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x6000,        // PDO entry index
        0x0F,          // PDO entry subindex
        &off_faults,
        &bit_position},          // bit position
    //MOSI
    {HCRLSlavePos,  HCRL_Slave, 
        0x7000,        // PDO entry index
        0x01,          // PDO entry subindex
        &off_loop,
        &bit_position},         // bit position
    {HCRLSlavePos,  HCRL_Slave, 
        0x7000,        // PDO entry index
        0x02,          // PDO entry subindex
        &off_current_des_raw,
        &bit_position},         // bit position
    {HCRLSlavePos,  HCRL_Slave,  // This is necessary to null terminate this array.
        0x00,                    // A PDO entry index of zero prevents ecrt_domain_reg_pdo_entry_list(...) from going beyond the end of this array.
        0x00,
        NULL,
        NULL}
};

// The code below is output from the following command: 
// $ sudo ethercat cstruct

/* Master 0, Slave 0, "HCRL-Axon"
 * Vendor ID:       0x000005ff
 * Product code:    0x26483052
 * Revision number: 0x00000008
 */

ec_pdo_entry_info_t slave_0_pdo_entries[] = {
    {0x7000, 0x01, 16}, /* Loop */
    {0x7000, 0x02, 16}, /* Current_des_raw */
    {0x6000, 0x01, 16}, /* LoopPlusOne */
    {0x6000, 0x02, 16}, /* ADC_v1_raw */
    {0x6000, 0x03, 16}, /* ADC_v2_raw */
    {0x6000, 0x04, 16}, /* ADC_v3_raw */
    {0x6000, 0x05, 16}, /* ADC_v4_raw */
    {0x6000, 0x06, 16}, /* ADC_v5_raw */
    {0x6000, 0x07, 16}, /* ADC_v6_raw */
    {0x6000, 0x08, 16}, /* ADC_v7_raw */
    {0x6000, 0x09, 16}, /* ADC_v8_raw */
    {0x6000, 0x0a, 16}, /* Hall_dt_low */
    {0x6000, 0x0b, 16}, /* Hall_dt_high */
    {0x6000, 0x0c, 16}, /* Hall_position_raw */
    {0x6000, 0x0d, 16}, /* MotorCoreTempEst_C_low */
    {0x6000, 0x0e, 16}, /* MotorCoreTempEst_C_high */
    {0x6000, 0x0f, 16}, /* Faults */
};

ec_pdo_info_t slave_0_pdos[] = {
    {0x1600, 2, slave_0_pdo_entries + 0}, /* MOSI process data mapping */
    {0x1a00, 15, slave_0_pdo_entries + 2}, /* MISO process data mapping */
};

ec_sync_info_t slave_0_syncs[] = {
    {0, EC_DIR_OUTPUT, 0, NULL, EC_WD_DISABLE},
    {1, EC_DIR_INPUT, 0, NULL, EC_WD_DISABLE},
    {2, EC_DIR_OUTPUT, 1, slave_0_pdos + 0, EC_WD_ENABLE},
    {3, EC_DIR_INPUT, 1, slave_0_pdos + 1, EC_WD_DISABLE},
    {0xff}
};


void check_domain1_state(void)
{
    ec_domain_state_t ds;

    ecrt_domain_state(domain1, &ds); // reads state of a domain, see line 1717 of ecrt.h

    if (ds.working_counter != domain1_state.working_counter)
        printf("Domain1: WC %u.\n", ds.working_counter);
    if (ds.wc_state != domain1_state.wc_state)
        printf("Domain1: State %u.\n", ds.wc_state);

    domain1_state = ds;
}

/*****************************************************************************/

void check_master_state(void)
{
    ec_master_state_t ms;

    ecrt_master_state(master, &ms); // reads state of the master, see line 994 of ecrt.h

    if (ms.slaves_responding != master_state.slaves_responding)
        printf("%u slave(s).\n", ms.slaves_responding);
    if (ms.al_states != master_state.al_states)
        printf("AL states: 0x%02X.\n", ms.al_states);
    if (ms.link_up != master_state.link_up)
        printf("Link is %s.\n", ms.link_up ? "up" : "down");

    // printf("check_master_state:\n"
    //        "  - number of slaves responding: %u\n"
    //        "  - application layer states: %u\n"
    //        "  - link up: %u\n", ms.slaves_responding, ms.al_states, ms.link_up);


    master_state = ms;
}

/*****************************************************************************/

void check_slave_config_states(void)
{
    ec_slave_config_state_t s;

    ecrt_slave_config_state(slave_config, &s);

    if (s.al_state != slave_config_state.al_state)
        printf("AnaIn: State 0x%02X.\n", s.al_state);
    if (s.online != slave_config_state.online)
        printf("AnaIn: %s.\n", s.online ? "online" : "offline");
    if (s.operational != slave_config_state.operational)
        printf("AnaIn: %soperational.\n",
                s.operational ? "" : "Not ");

    slave_config_state = s;
}

void updateLocalState(hcrl_axon_common::ACT *actMsg){

    unsigned int v1_raw;
    unsigned int v2_raw;
    unsigned int v3_raw;
    unsigned int v4_raw;
    unsigned int v5_raw;
    unsigned int v6_raw;
    unsigned int v7_raw;
    unsigned int v8_raw;
    unsigned int hall_dt_low;
    unsigned int hall_dt_high;
    int hall_dt;
    int hall_position_raw;
    unsigned int motorCoreTempEst_C_low;
    unsigned int motorCoreTempEst_C_high;
    int motorCoreTempEst_C_tmp;
    unsigned int faults;
    
    // read process data
    
    v1_raw = EC_READ_U16(domain1_pd + off_v1_raw);
    v2_raw = EC_READ_U16(domain1_pd + off_v2_raw);
    v3_raw = EC_READ_U16(domain1_pd + off_v3_raw);
    v4_raw = EC_READ_U16(domain1_pd + off_v4_raw);
    v5_raw = EC_READ_U16(domain1_pd + off_v5_raw);
    v6_raw = EC_READ_U16(domain1_pd + off_v6_raw);
    v7_raw = EC_READ_U16(domain1_pd + off_v7_raw);
    v8_raw = EC_READ_U16(domain1_pd + off_v8_raw);
    hall_dt_low = EC_READ_U16(domain1_pd + off_hall_dt_low);
    hall_dt_high = EC_READ_U16(domain1_pd + off_hall_dt_high);
    hall_position_raw = EC_READ_S16(domain1_pd + off_hall_position_raw);
    motorCoreTempEst_C_low = EC_READ_U16(domain1_pd + off_motorCoreTempEst_C_low);
    motorCoreTempEst_C_high = EC_READ_U16(domain1_pd + off_motorCoreTempEst_C_high);
    faults = EC_READ_U16(domain1_pd + off_faults);

    //get rid of compiler warning messages
    (void)v1_raw;

    //printf("CYCLIC_TASK: raw voltages: %u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t\n", v1_raw,v2_raw,v3_raw,v4_raw,v5_raw,v6_raw,v7_raw,v8_raw);
    actMsg->Resevoir_temp_F = rawADC_2_F(v8_raw);
    actMsg->MotorCase_temp_F = rawADC_2_F(v7_raw);
    actMsg->Servo_temp_F = rawADC_2_F(v6_raw);
    actMsg->Bus_voltage_V = rawADC_2_Vbus(v5_raw);
    actMsg->Bus_current_A = rawADC_2_Ibus(v4_raw);
    actMsg->Battery_power_W = actMsg->Bus_voltage_V * actMsg->Bus_current_A;

    hall_dt = (hall_dt_high<<16)|hall_dt_low;
    actMsg->Motor_vel_radps = deltaT_2_radps( hall_dt );
    actMsg->Motor_vel_rpm = deltaT_2_rpm( hall_dt );
    actMsg->Motor_angle_rad = rawHall_2_rad( hall_position_raw );
    actMsg->Motor_current_meas_A = rawADC_2_Imotor(v3_raw);
    actMsg->Motor_torque_nm = rawADC_2_Tmotor(v2_raw);
    actMsg->Motor_mech_power_W = actMsg->Motor_torque_nm * actMsg->Motor_vel_radps;

    motorCoreTempEst_C_tmp = (motorCoreTempEst_C_high<<16)|motorCoreTempEst_C_low;
    actMsg->MotorCore_temp_F = C_2_F( *(float *)((void *)(&motorCoreTempEst_C_tmp)) );
    actMsg->Faults = faults;

    actMsg->VoltRef2p5 = rawADC_2_volts(v1_raw);

}
